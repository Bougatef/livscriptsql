FROM boxfuse/flyway:latest

# copy driver
COPY  ./drivers /flyway/drivers
COPY  ./configSql /flyway/sql

ENTRYPOINT [ "/bin/sh", "-c" , "/bin/sleep 20 && flyway -user=${USER} -password=${PASSWORD} -url=${DSN} info" ]